package com.capgemini.fraudanalyser;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.fraudanalyser.transaction.Transaction;

public class FraudAnalyserTest {

	FraudAnalyser fraudAnalyser;
	Set<Transaction> transactions;

	Set<Long> restrictedUserId = new HashSet<Long>(Arrays.asList(101l, 606l));
	Set<Long> suspectedUserId = new HashSet<Long>(Arrays.asList(542l, 1052l, 2103l));
	Set<Long> expected;

	@Before
	public void setUp() throws Exception {
		fraudAnalyser = new FraudAnalyser();
		transactions = new HashSet<Transaction>();
		expected = new HashSet<Long>();
	}

	@Test
	public void shouldReturnNoneWhenRestrictedUserId() {
		transactions.add(new Transaction(1, 101, 980, new GregorianCalendar(2015, 12, 12), 5000000.0));
		transactions.add(new Transaction(2, 606, 231, new GregorianCalendar(2015, 2, 12), 500021430.0));
		int actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId).size();
		assertEquals(0, actual);
	}

	@Test
	public void shouldNotConsiderRestrictedUserIdWhenRestrictedUserIdInputEmpty() {
		restrictedUserId = new HashSet<Long>();
		transactions.add(new Transaction(1, 101, 980, new GregorianCalendar(2015, 12, 12), 5000.0));
		transactions.add(new Transaction(2, 101, 9820, new GregorianCalendar(2015, 12, 12), 5000.0));
		transactions.add(new Transaction(3, 101, 920, new GregorianCalendar(2015, 12, 12), 5.0));
		expected.addAll(Arrays.asList(1l, 2l, 3l));
		Set<Long> actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId);
		assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnNoneWhenRestrictedUserIdAndNormalTransaction() {
		transactions.add(new Transaction(1, 101, 980, new GregorianCalendar(2015, 12, 12), 5000000.0));
		transactions.add(new Transaction(2, 606, 231, new GregorianCalendar(2015, 2, 12), 500021430.0));
		transactions.add(new Transaction(3, 321, 2143, new GregorianCalendar(2012, 4, 12), 2000.0));
		int actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId).size();
		assertEquals(0, actual);
	}

	@Test
	public void shouldReturnNoneWhenNormalTransaction() {
		transactions.add(new Transaction(1, 1013, 980, new GregorianCalendar(2015, 12, 12), 5000000.0));
		transactions.add(new Transaction(2, 6063, 231, new GregorianCalendar(2015, 2, 12), 500021430.0));
		transactions.add(new Transaction(3, 3213, 2143, new GregorianCalendar(2012, 4, 12), 2000.0));
		int actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId).size();
		assertEquals(0, actual);
	}

	@Test
	public void shouldNotConsiderSuspectedUserIdWhenSuspectedUserIdInputEmpty() {
		suspectedUserId = new HashSet<Long>();
		transactions.add(new Transaction(1, 1013, 980, new GregorianCalendar(2015, 12, 12), 5000000.0));
		transactions.add(new Transaction(2, 6063, 231, new GregorianCalendar(2015, 2, 12), 500021430.0));
		transactions.add(new Transaction(3, 3213, 2143, new GregorianCalendar(2012, 4, 12), 2000.0));
		int actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId).size();
		assertEquals(0, actual);
	}

	@Test
	public void shouldReturnAllTransactionWithUserIdRestricted() {
		transactions.add(new Transaction(1, 542, 980, new GregorianCalendar(2015, 12, 12), 50.0));
		transactions.add(new Transaction(2, 1052, 231, new GregorianCalendar(2015, 2, 12), 530.0));
		transactions.add(new Transaction(3, 2103, 2143, new GregorianCalendar(2012, 4, 12), 500.0));
		transactions.add(new Transaction(4, 321, 2143, new GregorianCalendar(2012, 4, 22), 2000.0));
		transactions.add(new Transaction(5, 321, 2143, new GregorianCalendar(2012, 4, 12), 2100.0));
		transactions.add(new Transaction(6, 2103, 243, new GregorianCalendar(2012, 5, 121), 500.0));
		expected.addAll(Arrays.asList(1l, 2l, 3l, 6l));
		Set<Long> actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId);
		assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnAllTransactionWhenUserDoMoreThan5TransactionPerDay() {
		transactions.add(new Transaction(1, 532, 980, new GregorianCalendar(2015, 12, 12), 50.0));
		transactions.add(new Transaction(2, 321, 231, new GregorianCalendar(2015, 2, 12), 530.0));
		transactions.add(new Transaction(3, 321, 213, new GregorianCalendar(2015, 2, 12), 500.0));
		transactions.add(new Transaction(4, 321, 214, new GregorianCalendar(2015, 2, 12), 20.0));
		transactions.add(new Transaction(5, 321, 243, new GregorianCalendar(2015, 2, 12), 21.0));
		transactions.add(new Transaction(6, 321, 243, new GregorianCalendar(2015, 2, 12), 5.0));
		transactions.add(new Transaction(7, 321, 243, new GregorianCalendar(2015, 2, 12), 10.0));
		transactions.add(new Transaction(8, 321, 243, new GregorianCalendar(2015, 2, 13), 10.0));
		expected.addAll(Arrays.asList(2l, 3l, 4l, 5l, 6l, 7l));
		Set<Long> actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId);
		assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnNoneWhenUserNotDoMoreThan5TransactionPerDay() {
		transactions.add(new Transaction(2, 321, 231, new GregorianCalendar(2015, 2, 12), 530.0));
		transactions.add(new Transaction(3, 321, 213, new GregorianCalendar(2015, 2, 12), 500.0));
		transactions.add(new Transaction(4, 321, 214, new GregorianCalendar(2015, 2, 12), 20.0));
		transactions.add(new Transaction(5, 321, 243, new GregorianCalendar(2015, 2, 14), 21.0));
		transactions.add(new Transaction(6, 321, 243, new GregorianCalendar(2015, 2, 12), 5.0));
		transactions.add(new Transaction(7, 321, 243, new GregorianCalendar(2015, 2, 12), 10.0));
		transactions.add(new Transaction(7, 321, 243, new GregorianCalendar(2015, 2, 13), 10.0));
		int actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId).size();
		assertEquals(0, actual);
	}

	@Test
	public void shouldReturnAllTransactionWhenUserDoMoreThan4TransactionToSamePerDay() {
		transactions.add(new Transaction(1, 532, 980, new GregorianCalendar(2015, 2, 12), 50.0));
		transactions.add(new Transaction(2, 321, 214, new GregorianCalendar(2015, 2, 12), 530.0));
		transactions.add(new Transaction(3, 321, 214, new GregorianCalendar(2015, 2, 12), 500.0));
		transactions.add(new Transaction(4, 321, 214, new GregorianCalendar(2015, 2, 12), 20.0));
		transactions.add(new Transaction(5, 321, 214, new GregorianCalendar(2015, 2, 12), 21.0));
		transactions.add(new Transaction(6, 321, 214, new GregorianCalendar(2015, 2, 12), 5.0));
		transactions.add(new Transaction(7, 321, 214, new GregorianCalendar(2015, 2, 13), 10.0));
		expected.addAll(Arrays.asList(2l, 3l, 4l, 5l, 6l));
		Set<Long> actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId);
		assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnNoneWhenUserNotDoMoreThan4TransactionToSamePerDay() {
		transactions.add(new Transaction(1, 532, 980, new GregorianCalendar(2015, 2, 12), 50.0));
		transactions.add(new Transaction(2, 321, 214, new GregorianCalendar(2015, 2, 12), 530.0));
		transactions.add(new Transaction(3, 321, 214, new GregorianCalendar(2015, 2, 12), 500.0));
		transactions.add(new Transaction(4, 321, 214, new GregorianCalendar(2015, 2, 12), 20.0));
		transactions.add(new Transaction(5, 321, 214, new GregorianCalendar(2015, 2, 12), 21.0));
		transactions.add(new Transaction(6, 321, 213, new GregorianCalendar(2015, 2, 12), 5.0));
		transactions.add(new Transaction(7, 321, 214, new GregorianCalendar(2015, 2, 13), 10.0));
		int actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId).size();
		assertEquals(0, actual);
	}

	@Test
	public void shouldReturnAllTransactionWhenMoreThan3TransactionSumOver5000PerDay() {
		transactions.add(new Transaction(1, 532, 980, new GregorianCalendar(2015, 2, 12), 50.0));
		transactions.add(new Transaction(2, 321, 214, new GregorianCalendar(2015, 2, 12), 2530.0));
		transactions.add(new Transaction(3, 321, 214, new GregorianCalendar(2015, 2, 12), 2500.0));
		transactions.add(new Transaction(4, 321, 214, new GregorianCalendar(2015, 2, 12), 20.0));
		transactions.add(new Transaction(5, 321, 214, new GregorianCalendar(2015, 2, 12), 21.0));
		transactions.add(new Transaction(6, 321, 234, new GregorianCalendar(2015, 2, 13), 10.0));
		expected.addAll(Arrays.asList(2l, 3l, 4l, 5l));
		Set<Long> actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId);
		assertEquals(expected, actual);
	}
	
	@Test
	public void differentHoursShouldNotInfluentOnResoults() {
		transactions.add(new Transaction(1, 532, 980, new GregorianCalendar(2015, 2, 12), 50.0));
		transactions.add(new Transaction(2, 321, 214, new GregorianCalendar(2015, 2, 12, 13, 42), 2530.0));
		transactions.add(new Transaction(3, 321, 214, new GregorianCalendar(2015, 2, 12, 02, 21), 2500.0));
		transactions.add(new Transaction(4, 321, 214, new GregorianCalendar(2015, 2, 12, 23, 41), 20.0));
		transactions.add(new Transaction(5, 321, 214, new GregorianCalendar(2015, 2, 12, 14, 32), 21.0));
		transactions.add(new Transaction(6, 321, 234, new GregorianCalendar(2015, 2, 13), 10.0));
		expected.addAll(Arrays.asList(2l, 3l, 4l, 5l));
		Set<Long> actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId);
		assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnNoneWhenLessThan3TransactionSumOver5000PerDay() {
		transactions.add(new Transaction(1, 532, 980, new GregorianCalendar(2015, 2, 12), 50.0));
		transactions.add(new Transaction(2, 321, 214, new GregorianCalendar(2015, 2, 12), 2530.0));
		transactions.add(new Transaction(3, 321, 214, new GregorianCalendar(2015, 2, 12), 2500.0));
		transactions.add(new Transaction(4, 321, 214, new GregorianCalendar(2015, 2, 12), 20.0));
		transactions.add(new Transaction(5, 321, 214, new GregorianCalendar(2015, 2, 11), 21.0));
		transactions.add(new Transaction(6, 321, 214, new GregorianCalendar(2015, 2, 13), 10.0));
		int actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId).size();
		assertEquals(0, actual);
	}

	@Test
	public void shouldReturnAllTransactionWhenMoreThan2TransactionSumOver10000PerDay() {
		transactions.add(new Transaction(1, 532, 980, new GregorianCalendar(2015, 2, 12), 50.0));
		transactions.add(new Transaction(2, 321, 214, new GregorianCalendar(2015, 2, 12), 7530.0));
		transactions.add(new Transaction(3, 321, 234, new GregorianCalendar(2015, 2, 12), 2500.0));
		transactions.add(new Transaction(4, 321, 224, new GregorianCalendar(2015, 2, 12), 20.0));
		transactions.add(new Transaction(5, 321, 214, new GregorianCalendar(2015, 2, 13), 21.0));
		transactions.add(new Transaction(6, 321, 234, new GregorianCalendar(2015, 2, 13), 10.0));
		expected.addAll(Arrays.asList(2l, 3l, 4l));
		Set<Long> actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId);
		assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnNoneWhenLessThan2TransactionSumOver10000PerDay() {
		transactions.add(new Transaction(1, 532, 980, new GregorianCalendar(2015, 2, 12), 50.0));
		transactions.add(new Transaction(2, 321, 214, new GregorianCalendar(2015, 2, 12), 7530.0));
		transactions.add(new Transaction(3, 321, 214, new GregorianCalendar(2015, 2, 12), 2500.0));
		transactions.add(new Transaction(4, 321, 214, new GregorianCalendar(2015, 2, 15), 20.0));
		transactions.add(new Transaction(5, 321, 214, new GregorianCalendar(2015, 2, 11), 21.0));
		transactions.add(new Transaction(6, 321, 214, new GregorianCalendar(2015, 2, 13), 10.0));
		int actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId).size();
		assertEquals(0, actual);
	}

	@Test
	public void shouldReturnNoneWhenImputHaveNotTransaction() {
		int actual = fraudAnalyser.getSuspectedTransactionId(transactions, restrictedUserId, suspectedUserId).size();
		assertEquals(0, actual);
	}
}
