package com.capgemini.fraudanalyser.transaction;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Transaction {

	private long transactionId;
	private long userId;
	private long reciverId;
	private Calendar date;
	private double amount;
	private boolean suspected;

	public Transaction(long transactionId, long userId, long reciverId, Calendar date, double amount) {
		this.transactionId = transactionId;
		this.userId = userId;
		this.reciverId = reciverId;
		this.date = new GregorianCalendar(date.get(Calendar.YEAR), date.get(Calendar.MONTH),
				date.get(Calendar.DAY_OF_MONTH));
		this.amount = amount;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public long getUserId() {
		return userId;
	}

	public long getReciverId() {
		return reciverId;
	}

	public Calendar getDate() {
		return date;
	}

	public double getAmount() {
		return amount;
	}

	public boolean isSuspected() {
		return suspected;
	}

	public void setSuspected(boolean suspected) {
		this.suspected = suspected;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (transactionId ^ (transactionId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (transactionId != other.transactionId)
			return false;
		return true;
	}

}
