package com.capgemini.fraudanalyser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.capgemini.fraudanalyser.transaction.Transaction;

public class TransactionOfUserByDate {

	private long userId;
	private Calendar date;
	private List<Transaction> transactions = new ArrayList<Transaction>();
	private List<Long> recieversId = new ArrayList<Long>();
	private Map<Long, Long> recieversCount = new HashMap<Long, Long>();

	protected TransactionOfUserByDate(long userId, Calendar calendar) {
		this.userId = userId;
		this.date = calendar;
	}

	protected void addTransaction(Transaction transaction) {
		this.transactions.add(transaction);
		this.recieversId.add(transaction.getReciverId());
	}

	private void checkNumberOfTransaction() {
		if (transactions.size() > 5) {
			changeTransactionStatus();
		}
	}

	private void updateMaps() {
		for (int i = 0; i < transactions.size(); i++) {
			long tempRecieverId = transactions.get(i).getReciverId();
			if (recieversCount.containsKey(tempRecieverId)) {
				recieversCount.replace(tempRecieverId, recieversCount.get(tempRecieverId) + 1);
			} else {
				recieversCount.put(tempRecieverId, 1l);
			}
		}
	}

	private void checkNumberOfTransactionToSingleClient() {
		for (long key : recieversCount.keySet()) {
			if (recieversCount.get(key) > 4) {
				changeTransactionStatus(key);
			}
		}
	}

	private void checkSumOfTransaction() {
		double sum = 0d;
		for (Transaction transaction : transactions) {
			sum += transaction.getAmount();
		}

		if (sum > 5000d && transactions.size() > 3) {
			changeTransactionStatus();
		} else if (sum > 10000d && transactions.size() > 2) {
			changeTransactionStatus();
		}
	}

	private void changeTransactionStatus(long key) {
		for (Transaction transaction : transactions) {
			if (transaction.getReciverId() == key) {
				transaction.setSuspected(true);
			}
		}
	}

	private void changeTransactionStatus() {
		for (Transaction transaction : transactions) {
			transaction.setSuspected(true);
		}
	}

	private void checkTransaction() {
		checkNumberOfTransaction();
		updateMaps();
		checkNumberOfTransactionToSingleClient();
		checkSumOfTransaction();
	}

	protected Set<Long> addSuspectedTransaction() {
		Set<Long> suspectedTransaction = new HashSet<Long>();
		checkTransaction();
		for (Transaction transaction : transactions) {
			if (transaction.isSuspected()) {
				suspectedTransaction.add(transaction.getTransactionId());
			}
		}
		return suspectedTransaction;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + (int) (userId ^ (userId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransactionOfUserByDate other = (TransactionOfUserByDate) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}
}
