package com.capgemini.fraudanalyser;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.capgemini.fraudanalyser.transaction.Transaction;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

public class FraudAnalyserSystem {

	Set<Long> restrictedUserId;
	Set<Long> suspectedUserId;

	protected void setRestrictedUserId(Set<Long> restrictedUserId) {
		this.restrictedUserId = restrictedUserId;
	}

	protected void setSuspectedUserId(Set<Long> suspectedUserId) {
		this.suspectedUserId = suspectedUserId;
	}

	protected Set<Transaction> removeRestrictedUserIdTransaction(Set<Transaction> transactions) {
		return new HashSet<Transaction>(Collections2.filter(transactions, byRestrictedUserId));
	}

	Predicate<Transaction> byRestrictedUserId = new Predicate<Transaction>() {
		public boolean apply(Transaction transaction) {
			for (long userId : restrictedUserId) {
				if (transaction.getUserId() == userId) {
					return false;
				}
			}
			return true;
		}
	};

	private Set<Transaction> getSuspectedUserTransaction(Set<Transaction> transactions) {
		return new HashSet<Transaction>(Collections2.filter(transactions, byReturnSuspectedUserId));
	}

	Predicate<Transaction> byReturnSuspectedUserId = new Predicate<Transaction>() {
		public boolean apply(Transaction transaction) {
			for (long userId : suspectedUserId) {
				if (transaction.getUserId() == userId) {
					return true;
				}
			}
			return false;
		}
	};

	protected Set<Long> getSuspectedUserIdTransaction(Set<Transaction> transactions) {
		Set<Long> userIdSet = new HashSet<Long>();
		for (Transaction transaction : this.getSuspectedUserTransaction(transactions)) {
			userIdSet.add(transaction.getTransactionId());
		}
		return userIdSet;
	}

	protected Set<Transaction> removeSuspectedUserIdTransaction(Set<Transaction> transactions) {
		return new HashSet<Transaction>(Collections2.filter(transactions, byDeleteSuspectedUserId));
	}

	Predicate<Transaction> byDeleteSuspectedUserId = new Predicate<Transaction>() {
		public boolean apply(Transaction transaction) {
			for (long userId : suspectedUserId) {
				if (transaction.getUserId() == userId) {
					return false;
				}
			}
			return true;
		}
	};

	protected Map<Integer, TransactionOfUserByDate> groupTransaction(Set<Transaction> transactions) {
		Map<Integer, TransactionOfUserByDate> resoultSet = new HashMap<Integer, TransactionOfUserByDate>();
		for (Transaction transaction : transactions) {
			TransactionOfUserByDate tempTransaction = new TransactionOfUserByDate(transaction.getUserId(),
					transaction.getDate());
			if (!resoultSet.containsKey(tempTransaction.hashCode())) {
				resoultSet.put(tempTransaction.hashCode(), tempTransaction);
			}
			resoultSet.get(tempTransaction.hashCode()).addTransaction(transaction);
		}
		return resoultSet;
	}
}
