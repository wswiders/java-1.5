package com.capgemini.fraudanalyser;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.capgemini.fraudanalyser.transaction.Transaction;

public class FraudAnalyser {

	Set<Long> suspectedTransactionId = new HashSet<Long>();

	public Set<Long> getSuspectedTransactionId(Set<Transaction> transactions, Set<Long> restrictedUserId,
			Set<Long> suspectedUserId) {
		
		FraudAnalyserSystem fraudAnalyserSystem = new FraudAnalyserSystem();
		fraudAnalyserSystem.setRestrictedUserId(restrictedUserId);
		fraudAnalyserSystem.setSuspectedUserId(suspectedUserId);
		
		transactions = fraudAnalyserSystem.removeRestrictedUserIdTransaction(transactions);
		Set<Long> suspTransByRestrictedUser = fraudAnalyserSystem.getSuspectedUserIdTransaction(transactions);
		this.suspectedTransactionId.addAll(suspTransByRestrictedUser);
		transactions = fraudAnalyserSystem.removeSuspectedUserIdTransaction(transactions);
		
		Map<Integer, TransactionOfUserByDate> groupedTransaction = fraudAnalyserSystem.groupTransaction(transactions);
		
		for (int keyTrans : groupedTransaction.keySet()) {
			Set<Long> suspectedTransaction = groupedTransaction.get(keyTrans).addSuspectedTransaction();
			this.suspectedTransactionId.addAll(suspectedTransaction);
		}
		return this.suspectedTransactionId;
	}
}
